package pl.codementors;

import java.io.Serializable;

public class User implements Serializable {

    public String name;

    public String surname;

    public int dateOfBorn;

     public enum Sex {
         Male,
         Female
     }

     private Sex sex;

     public User (){

     }

     public User (String name, String surename, int dateOfBorn, Sex sex){
         this.name = name;
         this.surname = surename;
         this.dateOfBorn = dateOfBorn;
         this.sex=sex;

     }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        surname = surname;
    }

    public int getDateOfBorn() {
        return dateOfBorn;
    }

    public void setDateOfBorn(int dateofBorn) {
        dateOfBorn = dateOfBorn;
    }

    public Sex getSex()
    {
        return sex;
    }

    public void setSex(Sex sex) {

         this.sex = sex;
    }


}
