package pl.codementors.view;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.cell.ChoiceBoxTableCell;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.layout.Region;
import javafx.stage.FileChooser;
import javafx.util.converter.IntegerStringConverter;
import pl.codementors.User;

import java.io.*;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

public class UserList implements Initializable{

    private static final Logger log = Logger.getLogger(UserList.class.getCanonicalName());

    @FXML
    private TableView usersTable;

    @FXML
    private TableColumn<User, String> nameColumn;

    @FXML
    private TableColumn<User, String> surnameColumn;

    @FXML
    private TableColumn<User, Integer> dateOfBornColumn;

    @FXML
    private TableColumn<User, User.Sex> sexColumn;

    @FXML
    private ObservableList<User> users = FXCollections.observableArrayList();

    private ResourceBundle rb;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
    usersTable.setItems(users);
        rb = resourceBundle;

        nameColumn.setCellFactory(TextFieldTableCell.forTableColumn());
        nameColumn.setOnEditCommit(event -> event.getRowValue().setName(event.getNewValue()));

        surnameColumn.setCellFactory(TextFieldTableCell.forTableColumn());
        surnameColumn.setOnEditCommit(event -> event.getRowValue().setSurname(event.getNewValue()));

        dateOfBornColumn.setCellFactory(TextFieldTableCell.forTableColumn(new IntegerStringConverter()));
        dateOfBornColumn.setOnEditCommit(event -> event.getRowValue().setDateOfBorn(event.getNewValue()));

        sexColumn.setCellFactory(ChoiceBoxTableCell.forTableColumn(User.Sex.values()));
        sexColumn.setOnEditCommit(event -> event.getRowValue().setSex(event.getNewValue()));
    }

    @FXML
    private void open(ActionEvent event){
        FileChooser chooser = new FileChooser();
        File file = chooser.showOpenDialog(((MenuItem)event.getTarget()).getParentPopup().getScene().getWindow());
        if (file != null) {
            open(file);
        }
    }

    private void open(File file) {
        try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(file))) {
            User[] loadedMages = (User[]) ois.readObject();
            users.addAll(loadedMages);
        } catch (IOException | ClassNotFoundException ex) {
            log.log(Level.WARNING, ex.getMessage(), ex);
        }
    }

    @FXML
    private void save(ActionEvent event) {
        FileChooser chooser = new FileChooser();
        File file = chooser.showSaveDialog(((MenuItem)event.getTarget()).getParentPopup().getScene().getWindow());
        if (file != null) {
            save(file);
        }
    }

    private void save (File file){
        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(file))) {
            oos.writeObject(users.toArray(new User[0]));
        } catch (IOException ex) {
            log.log(Level.WARNING, ex.getMessage(), ex);
        }
    }

    @FXML
    private void about(ActionEvent event) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle(rb.getString("aboutTitle"));
        alert.setHeaderText(rb.getString("aboutHeader"));
        alert.setContentText(rb.getString("aboutContent"));
        alert.getDialogPane().setMinHeight(Region.USE_PREF_SIZE);
        alert.showAndWait();
    }

    @FXML
    private void add(ActionEvent event) {
        users.add(new User());
    }

    @FXML
    private void delete(ActionEvent event) {
        if (usersTable.getSelectionModel().getSelectedIndex() >= 0) {
            users.remove(usersTable.getSelectionModel().getSelectedIndex());
            usersTable.getSelectionModel().clearSelection();
        }
    }
}


